<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>

</head>
<body>

<div id="container">
	<h1>Welcome to CodeIgniter!</h1>


	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script>
		$(window).on("scroll", function(e){
		    console.log(e);
		    window.debugVar = e ;
		    $.ajax({
				method: "post",
			 	url: "/api",
			 	data: { data: JSON.stringify(e.timestamp) }
			})
			.done(function( msg ) {
				alert( "data saved: " + msg );
			});  
		});
	</script>
</div>

</body>
</html>