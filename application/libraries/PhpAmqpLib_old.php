<?php if (!defined("BASEPATH")) exit("No direct script access allowed");

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Message\AMQPMessage;

class PhpAmqpLib {

	private $init = FALSE;

	private $config;
	private $connection;
	private $channel;
	private $status;
	
	public $queue;

	public function __construct($params = array())
	{
		$CI =& get_instance();
		$CI->config->load('phpamqplib', TRUE);
		$this->config = $CI->config->item('phpamqplib');

		$this->connection = new AMQPStreamConnection(
				 $this->config['host'],
				 $this->config['port'],
				 $this->config['user'],
				 $this->config['pass']
		);
		// $this->connection->connect();
		$this->channel = new AMQPChannel($this->connection);
		// $this->channel->setPrefetchCount(0);

		if(isset($params['queue_name']))
		{
			$queue_name = $params['queue_name'];
		}
		else
		{
			$queue_name = $this->config['queue_name'];
		}

		// $this->queue = new AMQPQueue($this->channel);
		// $this->queue->setName($queue_name);
		// $this->queue->declareQueue();
	}

	public function push($route, $data = array())
	{
		if(is_array($data))
		{
			$message = json_encode($data);
		}
		else
		{
			$message = $data;
		}

		$this->queue->bind($this->config['exchange_name'], $route);
		$exchange = new AMQPExchange($this->channel);
		$exchange->setName($this->config['exchange_name']);
		$exchange->publish($message, $route, 0, array(
			'message_id' => random_string(16, TRUE)
		));
	}

	public function put()
	{
		return $this->queue;
	}

	public function fail($message)
	{
		$msg_id = $message['message_id'];
		$delivery_tag = $message['delivery_tag'];
		if(!isset($this->status[$msg_id]))
		{
			$this->status[$msg_id] = array(
				'attempts' => 1,
				'worker_id' => 1
			);
		}
		else
		{
			$this->status[$msg_id]['attempts'] += 1;
			$this->status[$msg_id]['worker_id'] = 1;
		}
		
		if($this->status[$msg_id]['attempts'] > 2)
		{
			$this->queue->ack($delivery_tag);
		}
		else
		{
			$this->queue->nack($delivery_tag, AMQP_REQUEUE);
		}			
	}

	public function success($message)
	{
		$delivery_tag = $message['delivery_tag'];
		$this->queue->ack($delivery_tag);
	}

	public function message_status($msg_id)
	{
		if(isset($this->status[$msg_id]))
		{
			return $this->status[$msg_id];
		}
	}

	public function clear_queue()
	{
		$this->queue->purge();
	}

	public function __destruct() {

		if($this->connection)
		{
			$this->connection->disconnect();
		}
		
	}

}