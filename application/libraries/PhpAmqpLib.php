<?php if (!defined("BASEPATH")) exit("No direct script access allowed");

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Message\AMQPMessage;

class PhpAmqpLib {

	protected $CI;

	private $config;
	private $connection;
	private $channel;
	private $callback;

	private $queue_name;
	private $exchange_name;

	public function __construct($params = array())
	{
		$this->CI =& get_instance();
		$this->CI->config->load('phpamqplib', TRUE);
		$this->config = $this->CI->config->item('phpamqplib');

		$this->connection = new AMQPStreamConnection(
			$this->config['host'],
			$this->config['port'],
			$this->config['user'],
			$this->config['pass']
		);
		$this->channel = $this->connection->channel();

		$this->exchange_name = isset($params['exchange_name']) ?
		$params['exchange_name'] : $this->config['exchange_name'];

		$this->channel->exchange_declare($this->exchange_name, 'direct', false, false, false);
	}

	public function push($severity, $data)
	{
		if(is_array($data))
		{
			$data = json_encode($data);
		}
		$msg = new AMQPMessage($data);

		$this->channel->basic_publish($msg, $this->exchange_name, $severity);
	}


	public function set_callback($class, $method)
	{
		$this->callback = array($class, $method);
	}	


	public function listen($severities)
	{		
		list($this->queue_name, ,) = $this->channel->queue_declare('queue'.$severities, false, true, false, false);
		$this->channel->basic_qos(null, 1, null);
		$this->channel->basic_consume($this->queue_name, '', false, false, false, false, $this->callback);

		$severities = explode('|', $severities);
		foreach($severities as $severity) {
		    $this->channel->queue_bind($this->queue_name, $this->exchange_name, $severity);
		}

		while(count($this->channel->callbacks)) {
		    $this->channel->wait();
		}
	}


	public function __destruct()
	{
		if($this->channel)
		{
			$this->channel->close();
		}

		if($this->connection)
		{
			$this->connection->close();
		}
	}


}