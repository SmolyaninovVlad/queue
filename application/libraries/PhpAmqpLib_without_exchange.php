<?php if (!defined("BASEPATH")) exit("No direct script access allowed");

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Message\AMQPMessage;

class PhpAmqpLib {

	protected $CI;

	private $config;
	private $connection;
	private $channel;
	private $callback;

	private $queue_name;

	public function __construct($params = array())
	{
		$this->CI =& get_instance();
		$this->CI->config->load('phpamqplib', TRUE);
		$this->config = $this->CI->config->item('phpamqplib');

		$this->connection = new AMQPStreamConnection(
			$this->config['host'],
			$this->config['port'],
			$this->config['user'],
			$this->config['pass']
		);
		$this->channel = $this->connection->channel();

		$this->queue_name = isset($params['queue_name']) ?
		$params['queue_name'] : $this->config['queue_name'];

		$this->channel->queue_declare($this->queue_name, false, true, false, false);
	}

	public function push($data)
	{
		if(is_array($data))
		{
			$data = json_encode($data);
		}
		$msg = new AMQPMessage($data, array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT));

		$this->channel->basic_publish($msg, '', $this->queue_name);
	}


	public function set_callback($class, $method)
	{
		$this->callback = array($class, $method);
	}	


	public function listen()
	{		
		$this->channel->basic_qos(null, 1, null);
		$this->channel->basic_consume($this->queue_name, '', false, false, false, false, $this->callback);

		while(count($this->channel->callbacks)) {
		    $this->channel->wait();
		}
	}


	public function __destruct()
	{
		if($this->channel)
		{
			$this->channel->close();
		}

		if($this->connection)
		{
			$this->connection->close();
		}
	}


}