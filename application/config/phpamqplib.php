<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| AMQP Server Configuration
| -------------------------------------------------------------------------
| This determine your message broker (server) configuration
|
*/

$config['host'] = 'localhost';
$config['port'] = 5672;
$config['user'] = 'guest';
$config['pass'] = 'guest';

$config['queue_name'] = 'direct_messages';
$config['exchange_name'] = 'amq.direct';

/* End of file amqp.php */
/* Location: ./application/config/amqp.php */