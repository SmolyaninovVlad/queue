<?php
 // if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Queue extends CI_Controller {

	private $queue;
	private $message;

	public function __construct()
	{
		parent::__construct();
		$this->output->enable_profiler(FALSE);

		if(php_sapi_name() != 'cli')
		{
			show_404();
		}

		set_time_limit(0);
		ini_set('memory_limit', '512M');

		$this->load->library('PhpAmqpLib', array(
			'queue_name' => 'links'
		));

		$this->queue = $this->phpamqplib->queue;
		while(true)
		{
			$this->queue->consume(array($this, 'router'));
		}
	}

	public function router($msg)
	{
		$data = json_decode($msg->getBody(), TRUE);
		$route = $msg->getRoutingKey();

		printf("[".date('Y-m-d H:i:s')."] " . $route . "\n");

		$this->message = array();
		// if(method_exists($this, $route))
		// {
		// 	$this->message['delivery_tag'] = $msg->getDeliveryTag();
		// 	$this->message['message_id'] = $msg->getMessageId();
		// 	call_user_func(array($this, $route), $data);
		// }
	}

	public function Board_Created($data)
	{
		$this->load->model('users_model', 'users');

		if($this->users->create_followers_to_board($data['author_id'], $data['board_id']))
		{
			$this->phpamqplib->success($this->message);
		}
		else
		{
			$this->phpamqplib->fail($this->message);
		}
	}

	public function Link_Creation($data)
	{
		$this->load->model('urls_model', 'urls');

		$insert_data = array(
			'url' => $data['url']
		);
		if(isset($data['site_id']))
		{
			$insert_data['site_id'] = $data['site_id'];
		}
		if($url_id = $this->urls->create_simple($insert_data))
		{
			$msg = json_encode(array(
				'id' => $url_id,
				'url' => $data['url']
			));
			$this->phpamqplib->push('Link_Parsing', $msg);
		}
	}

	public function Link_Creation_RSS($data)
	{
		$this->load->model('urls_model', 'urls');

		$insert_data = array(
			'url' => $data['url'],
			'site_id' => $data['site_id']
		);
		if($url_id = $this->urls->create_simple($insert_data))
		{
			$msg = json_encode(array(
				'id' => $url_id,
				'url' => $data['url']
			));
			$this->phpamqplib->push('Link_Parsing', $msg);
		}
	}

	public function Link_Parsing($data)
	{
		$this->load->library('URLParser');
		$this->load->model('urls_model', 'urls');

		if(!isset($data['url']) OR !isset($data['id'])) 
		{
			$this->phpamqplib->fail($this->message);
			return;
		}

		$this->urlparser->clear();
		$this->urlparser->set($data['url']);
		$this->urlparser->run();

		if($this->urlparser->errors) 
		{
			$this->phpamqplib->fail($this->message);
			return;
		}

		$this->load->helper('text');
		$url_data = $this->urlparser->get_data();
		$update = array(
			'title' => character_limiter(strip_tags(stripWhitespaces($url_data['title'])), 200, '...'),
			'description' => character_limiter(strip_tags(stripWhitespaces($url_data['description'])), 200, '...')
		);
		if(strlen($url_data['image']) < 255)
		{
			$update['image_original'] = $url_data['image'];
		}
		if(isset($update['image_original']) AND $update['image_original'])
		{
			$msg = json_encode(array(
				'id' => $data['id'],
				'url' => $update['image_original']
			));
			$this->phpamqplib->push('Link_Images', $msg);
		}
		try {
			$this->urls->edit($update, (int)$data['id']);
		}
		catch(Exception $e)
		{
			printf("Error: %s\n", $e->getMessage());
		}
		$this->phpamqplib->success($this->message);
	}


	public function Site_Parsing($data)
	{
		$this->load->model('sites_model', 'sites');

		if(!isset($data['url']) OR !isset($data['id'])) 
		{
			$this->phpamqplib->fail($this->message);
			return;
		}

		if($site_data = $this->sites->parse($data['url']))
		{
			if(count($site_data['rss']) > 0)
			{
				$rss_feeds = array();
				foreach($site_data['rss'] as $rss_url)
				{
					$rss_feeds[] = array(
						'url' => $rss_url,
						'site_id' => $data['id']
					);
				}
				$this->load->model('rss_feeds_model', 'rss_feeds');
				$this->rss_feeds->insert_batch($rss_feeds);
			}
			unset($site_data['rss']);
			$this->sites->edit($site_data, $data['id']);
		}
		else
		{
			$this->phpamqplib->fail($this->message);
			return;
		}
		$this->phpamqplib->success($this->message);
	}

	public function Link_Images($data)
	{
		$this->load->model('upload_model', 'uploads');
		$this->load->model('urls_model', 'urls');

		$subdir = date('j/n');
		$filename = '';
		$config = array(
			'path' => 'links/original/' . $subdir,
			'name' => random_string(16)
		);
		$res = $this->uploads->save_remote_image($data['url'], $filename, $config);

		$config_resize = array(
			'path' => 'links',
			'resize' => TRUE,
			'sizes' => array(
					array('width'=>'1024', 'height'=>'768', 'alias'=>'1024x768/' . $subdir),
					array('width'=>'800', 'height'=>'600', 'alias'=>'800x600/' . $subdir),
					array('width'=>'600', 'height'=>'450', 'alias'=>'600x450/' . $subdir),
					array('width'=>'400', 'height'=>'300', 'alias'=>'400x300/' . $subdir),
					array('width'=>'200', 'height'=>'150', 'alias'=>'200x150/' . $subdir)
				),
			'name' => $filename
		);
		$this->uploads->do_good_resize($filename, $config_resize, 'original/' . $subdir . '/' . $filename);
		$this->urls->edit(array('image' => $subdir . '/' . $filename), $data['id']);
		$this->phpamqplib->success($this->message);
	}

	public function Site_Images()
	{

	}
}

/* End of file queue.php */
/* Location: ./application/controllers/queue.php */