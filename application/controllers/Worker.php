<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Worker extends CI_Controller {

    public function index($severities)
    {
        $this->load->library('PhpAmqpLib', array(
            'exchange_name' => 'test'
            ));

        $this->phpamqplib->set_callback($this, 'api');
        $this->phpamqplib->listen($severities);   
    }

    public function api($msg){
        echo " [x] Received ", $msg->body, "\n";
        $data = array(
            'site_id' => '1',
            'client_id' => '1',
            'data' => $msg->body
        );
        $this->db->insert('log', $data);
        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
    }
}
