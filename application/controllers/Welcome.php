<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    public function index()
    {
        $this->load->view('index');
    }

    public function api()
    {
        $data = json_decode($this->input->post('data'));

        $this->load->library('PhpAmqpLib', array(
            'exchange_name' => 'test'
        ));
        
        $this->phpamqplib->push('logs',$data);
    }

}
